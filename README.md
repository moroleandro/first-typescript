<h3 align="center">
    <img alt="Logo" title="#logo" width="300px" src="https://cms-assets.tutsplus.com/uploads/users/71/courses/1221/preview_image/typescript-fundamentals-400x277.jpg">
    <br><br>
    <b>First Commit with TypeScript</b> 
    <p>Post sobre este repositório no <a href="https://medium.com/@moroleandro/entendendo-um-pouco-sobre-o-typescript-b5d8581e4d7" target="_blank">Medium</p>
</h3>

<a id="use"></a>

## :fire: Como usar

- ### **Pré-requisitos**

  - Possuir o **[Node.js](https://nodejs.org/en/)** instalado na máquina
  - Ter um gerenciador de pacotes seja o **[NPM](https://www.npmjs.com/)** ou **[Yarn](https://yarnpkg.com/)**.

1. Faça um clone desse repository :

```sh
  $ git clone https://gitlab.com/moroleandro/first-typescript.git
```

2. Executando a Aplicação:

```sh
  # Instale as dependências
  $ npm install

  ## Build
  $ npm run tsc 

  ## Start
  $ node src/index.js

```

---

<h4 align="center">
    Feito por <a href="https://www.linkedin.com/in/moroleandro/" target="_blank">Leandro Moro</a>
</h4>
